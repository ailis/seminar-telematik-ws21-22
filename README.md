# Seminar-Telematik-WS21-22
Git von Ailis Oßwald, Matrikelnummer: 4997184

Thema: **Datenschutz und Sicherheits Labels für das Internet der Dinge im Bezug auf Konsumenten**
- Was ist das Ziel solcher Label?
- Wie kann das Ziel erreicht werden?
- Welche Eigenschaften sind dafür hilfreich?
- Wie verständlich sind die Label für Konsumenten?

# Links
- Overleaf Latex Dokument: https://www.overleaf.com/read/ttcchzyvpbsg
- Ordner mit allen Präsentationen und Dokumenten die im Verlauf des Seminars entstehen: https://drive.google.com/drive/folders/1l8F5Dbb6GqbOVVvLiSV0yZyx7S5w36HB?usp=sharing
- Miro Board für Mindmaps & Denkstützen: https://miro.com/app/board/o9J_lnJXsOw=/?invite_link_id=281055718168

Alle hier gelisteten Links werden mindestens für 3 Jahre aktuell und erreichbar bleiben, soweit ich es beeinflussen kann. 

